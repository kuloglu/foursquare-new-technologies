package com.kuloglu.tech.di.component

import android.content.Context
import android.content.SharedPreferences
import com.kuloglu.tech.App
import com.kuloglu.tech.di.module.ApplicationModule
import com.kuloglu.tech.di.module.NetModule
import com.kuloglu.tech.ui.main.MainActivityViewModel
import com.kuloglu.tech.ui.main.venueDetail.VenueDetailViewModel
import com.kuloglu.tech.ui.main.venueList.VenueListViewModel
import dagger.Component
import javax.inject.Singleton


@Singleton

@Component(modules = [ApplicationModule::class, NetModule::class])


interface ApplicationComponent {
    fun app(): App


    fun context(): Context

    fun preferences(): SharedPreferences

    fun inject(mainActivityViewModel: MainActivityViewModel)
    fun inject(mainActivityViewModel: VenueDetailViewModel)
    fun inject(venueListViewModel: VenueListViewModel)
}
