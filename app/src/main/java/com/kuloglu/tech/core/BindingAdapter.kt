package com.kuloglu.tech.core

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.kuloglu.tech.BuildConfig
import com.kuloglu.tech.R
import com.squareup.picasso.Picasso


object BindingAdapter {

    @JvmStatic
    @BindingAdapter("app:set_list")
    fun setList(recyclerView: RecyclerView, list: List<Nothing>?) {
        val adapter = recyclerView.adapter as BaseAdapter<*>?
        list?.let {
            adapter?.submitList(null)
            adapter?.submitList(list)
        }
    }

    @JvmStatic
    @BindingAdapter("app:set_src")
    fun setSrc(imageView: ImageView, path: String) {
        if (path.isNotEmpty()) {
            Picasso.get()
                    .load(BuildConfig.PHOTO_API_URL + path)
                    .placeholder(R.drawable.progress_animaiton)
                    .error(R.drawable.error_black_24dp)
                    .into(imageView)
        }
    }

    @JvmStatic
    @BindingAdapter("app:loading_state")
    fun setLoadingState(extendedRecyclerView: ExtendedRecyclerView, isLoading: Boolean) {
        extendedRecyclerView.setLoadingState(isLoading)
    }


    @JvmStatic
    @BindingAdapter("app:visibility")
    fun setVisibilty(view: View, isVisible: Boolean) {
        view.visibility = View.GONE
        if (isVisible) {
            view.visibility = View.VISIBLE
        } else {
            view.visibility = View.GONE
        }
    }
}
