package com.kuloglu.tech.core

import com.kuloglu.tech.model.apiModel.venueList.Item

interface ClickListener {
    fun itemClick(item: Item)
}