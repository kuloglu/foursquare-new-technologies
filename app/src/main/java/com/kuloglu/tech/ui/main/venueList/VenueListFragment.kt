package com.kuloglu.tech.ui.main.venueList


import android.content.Context
import androidx.databinding.Observable
import androidx.recyclerview.widget.DividerItemDecoration
import com.kuloglu.tech.R
import com.kuloglu.tech.core.BaseFragment
import com.kuloglu.tech.core.ClickListener
import com.kuloglu.tech.databinding.FragmentVenueListBinding
import com.kuloglu.tech.ui.main.MainActivity


class VenueListFragment : BaseFragment<VenueListViewModel, FragmentVenueListBinding>(VenueListViewModel::class.java) {

    lateinit var clickListener: ClickListener
    override fun afterCreateView() {
        binding.venueList.adapter = VenueListAdapter {
            clickListener.itemClick(it)
        }
        binding.venueList.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))
        binding.venueList.setLoadingView(binding.progressBar)
        binding.venueList.setEmptyView(binding.emptyView)
        viewModel.title.addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                (activity as MainActivity).supportActionBar?.title = viewModel.title.get()
            }
        })
        viewModel.getResults()
    }

    override fun getLayoutRes() = R.layout.fragment_venue_list

    override fun initViewModel(viewModel: VenueListViewModel) {
        binding.viewModel = viewModel
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        clickListener = activity as ClickListener
    }

    fun searchSubmit(query: String) {
        viewModel.searchQuery.onNext(query)
    }


}
