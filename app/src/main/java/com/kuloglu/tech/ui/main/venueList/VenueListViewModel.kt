package com.kuloglu.tech.ui.main.venueList

import android.app.Application
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableField
import androidx.databinding.ObservableList
import com.kuloglu.tech.App
import com.kuloglu.tech.core.BaseViewModel
import com.kuloglu.tech.model.apiModel.venueList.FoursquareResponse
import com.kuloglu.tech.model.apiModel.venueList.Item
import com.kuloglu.tech.service.FoursquareService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import timber.log.Timber
import javax.inject.Inject

class VenueListViewModel(app: Application) : BaseViewModel(app) {
    val venueList: ObservableList<Item> = ObservableArrayList()
    private val compositeDisposable = CompositeDisposable()

    val loadingState: ObservableField<Boolean> = ObservableField(true)
    val title: ObservableField<String> = ObservableField("")

    /**
     *You must click search button from keyboard for search.
     */
    val searchQuery: BehaviorSubject<String> = BehaviorSubject.create()

    @Inject
    lateinit var api: FoursquareService

    init {
        (app as? App)?.component?.inject(this)
        compositeDisposable.add(
                searchQuery.subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread()).onErrorReturn { error ->
                            Timber.e(error.localizedMessage)
                            null
                        }.subscribe {
                            getResults(it)
                        })
    }

    fun getResults(q: String = "kebap") {
        title.set(q)
        loadingState.set(true)
        venueList.clear()
        compositeDisposable.add(api.getNearVenues(q, "Istanbul", "41.021024,28.941112")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn { error ->
                    Timber.e(error.localizedMessage)
                    loadingState.set(false)
                    FoursquareResponse(null)
                }.subscribe {
                    it.response?.groups?.get(0)?.items?.let { it1 -> venueList.addAll(it1) }
                    loadingState.set(false)
                })
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

}