package com.kuloglu.tech.ui.main.venueDetail

import android.app.Application
import androidx.databinding.ObservableField
import com.kuloglu.tech.App
import com.kuloglu.tech.R
import com.kuloglu.tech.core.BaseViewModel
import com.kuloglu.tech.model.VenueDetail
import com.kuloglu.tech.model.apiModel.venueDetail.Response
import com.kuloglu.tech.model.apiModel.venueDetail.ResponseVenueDetail
import com.kuloglu.tech.model.apiModel.venueDetail.Venue
import com.kuloglu.tech.service.FoursquareService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

class VenueDetailViewModel(app: Application) : BaseViewModel(app) {
    @Inject
    lateinit var api: FoursquareService

    val venueDetail: ObservableField<VenueDetail> = ObservableField(VenueDetail("", "", "", ""))

    private val compositeDisposable = CompositeDisposable()

    init {
        getApplication<App>().component.inject(this)
    }

    fun setViews(id: String?) {
        id?.let {
            api.getVenueDetail(it).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                    .onErrorReturn { error ->
                        Timber.e(error.localizedMessage)
                        ResponseVenueDetail(Response(null))
            }.subscribe { venue ->
                        venue.response.venue?.apply {
                            val photo = this.photos.groups[1].items[0].suffix
                            val formattedAddress = formattedAddress(this)
                            venueDetail.set(VenueDetail(
                                    name,
                                    photo,
                                    formattedAddress,
                                    rating.toString()
                            ))
                        }
            }
        }?.let {
            compositeDisposable.add(
                    it)
        }
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    private fun formattedAddress(venue: Venue): String {
        val sb = StringBuilder()



        venue.location?.let { location ->

            if (location.formattedAddress.isNotEmpty()) {
                location.formattedAddress.forEach {
                    sb.append("$it ")
                }
            } else {
                location.address?.let { address -> sb.append("$address ") }
                location.city?.let { city -> sb.append("$city ") }
                location.country?.let { country -> sb.append("$country ") }
            }
        }

        if (sb.isBlank()) {
            sb.append((getApplication() as App).getString(R.string.blank_adress))
        }
        return sb.toString()

    }


}