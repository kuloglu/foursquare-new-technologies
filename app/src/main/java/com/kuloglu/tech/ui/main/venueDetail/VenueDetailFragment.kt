package com.kuloglu.tech.ui.main.venueDetail

import android.os.Bundle
import com.kuloglu.tech.R
import com.kuloglu.tech.core.BaseFragment
import com.kuloglu.tech.databinding.FragmentVenueDetailBinding
import com.kuloglu.tech.model.apiModel.venueList.Item

class VenueDetailFragment : BaseFragment<VenueDetailViewModel, FragmentVenueDetailBinding>(VenueDetailViewModel::class.java) {
    override fun afterCreateView() {}

    override fun initViewModel(viewModel: VenueDetailViewModel) {
        binding.viewModel = viewModel
    }

    override fun getLayoutRes() = R.layout.fragment_venue_detail


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            arguments!!.getParcelable<Item>("item")?.let { viewModel.setViews(it.venue?.id) }
        }
    }
}