package com.kuloglu.tech.ui.main.venueList

import androidx.databinding.ObservableField
import com.kuloglu.tech.App
import com.kuloglu.tech.core.BaseViewModel
import com.kuloglu.tech.model.VenueItem
import com.kuloglu.tech.model.apiModel.venueList.Item

class VenueListItemViewModel(app: App) : BaseViewModel(app) {

    val venueItem: ObservableField<VenueItem> = ObservableField(VenueItem("", "", ""))
    lateinit var venue: Item

    fun setItem(item: Item) {
        val venueItem = VenueItem("", "", "")

        item.venue?.name?.let { venueItem.name = it }
        item.venue?.location?.address?.let { venueItem.address = it }
        item.venue?.id?.let { venueItem.id = it }

        this.venueItem.set(venueItem)
        venue = item
    }
}