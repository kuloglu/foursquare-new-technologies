package com.kuloglu.tech.ui.main

import android.app.Application
import android.os.Bundle
import androidx.fragment.app.FragmentManager
import com.kuloglu.tech.R
import com.kuloglu.tech.core.BaseViewModel
import com.kuloglu.tech.model.apiModel.venueList.Item
import com.kuloglu.tech.ui.main.venueDetail.VenueDetailFragment


class MainActivityViewModel(app: Application) : BaseViewModel(app) {
    var isTwoPane: Boolean = false

    fun itemClick(item: Item, supportFragmentManager: FragmentManager) {
        val transaction = supportFragmentManager.beginTransaction()
        if (!isTwoPane)
            transaction.add(R.id.rootView, getVenueDetailFragmentWithArgument(item), item.venue?.id)
        else
            transaction.add(R.id.detailContainer, getVenueDetailFragmentWithArgument(item), item.venue?.id)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    private fun getVenueDetailFragmentWithArgument(item: Item): VenueDetailFragment {
        val venueDetailFragment = VenueDetailFragment()
        val bundle = Bundle()
        bundle.putParcelable("item", item)
        venueDetailFragment.arguments = bundle
        return venueDetailFragment
    }

}