package com.kuloglu.tech.ui.main


import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.FragmentManager
import com.kuloglu.tech.R
import com.kuloglu.tech.core.BaseActivity
import com.kuloglu.tech.core.ClickListener
import com.kuloglu.tech.databinding.ActivityMainBinding
import com.kuloglu.tech.model.apiModel.venueList.Item
import com.kuloglu.tech.ui.main.venueList.VenueListFragment
import com.kuloglu.tech.utils.extensions.hideKeyboard


class MainActivity : BaseActivity<MainActivityViewModel, ActivityMainBinding>(MainActivityViewModel::class.java), ClickListener {

    lateinit var venueListFragment: VenueListFragment

    override fun itemClick(item: Item) {
        binding.viewModel?.itemClick(item, supportFragmentManager)
        if (!viewModel.isTwoPane) {
            menu?.findItem(R.id.menu_search)?.isVisible = false
            supportActionBar?.setHomeButtonEnabled(true)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
        }
        hideKeyboard()
    }

    private var menu: Menu? = null
    private lateinit var fragmentManager: FragmentManager

    override fun initViewModel(viewModel: MainActivityViewModel) {
        binding.viewModel = viewModel
        viewModel.isTwoPane = binding.detailContainer != null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        fragmentManager = supportFragmentManager

        if (savedInstanceState == null) {
            venueListFragment = VenueListFragment()
            fragmentManager.beginTransaction()
                    .add(R.id.rootView, venueListFragment)
                    .commit()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        this.menu = menu
        menuInflater.inflate(R.menu.menu_item, menu)
        val searchItem = menu.findItem(R.id.menu_search)
        var searchView: SearchView? = null
        searchItem?.let { searchView = it.actionView as SearchView }

        searchView!!.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                query?.let { venueListFragment.searchSubmit(it) }
                hideKeyboard()
                return true
            }

            override fun onQueryTextChange(newText: String?) = true
        })
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }


    override fun getLayoutRes() = R.layout.activity_main

    override fun onBackPressed() {
        super.onBackPressed()
        menu?.findItem(R.id.menu_search)?.isVisible = true
        supportActionBar?.setHomeButtonEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(false)

    }
}
