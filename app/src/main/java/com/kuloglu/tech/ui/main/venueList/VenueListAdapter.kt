package com.kuloglu.tech.ui.main.venueList

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import com.kuloglu.tech.App
import com.kuloglu.tech.R
import com.kuloglu.tech.core.BaseAdapter
import com.kuloglu.tech.databinding.ItemVenueBinding
import com.kuloglu.tech.model.apiModel.venueList.Item

class VenueListAdapter(private val clickCallback: ((Item) -> Unit)) : BaseAdapter<Item>(object : DiffUtil.ItemCallback<Item>() {
    override fun areItemsTheSame(oldItem: Item, newItem: Item) = oldItem.venue?.name == newItem.venue?.name
    override fun areContentsTheSame(oldItem: Item, newItem: Item) = oldItem == newItem
}) {
    override fun createBinding(parent: ViewGroup, viewType: Int): ViewDataBinding {
        val viewModel = VenueListItemViewModel(parent.context.applicationContext as App)
        val binding = DataBindingUtil.inflate<ItemVenueBinding>(LayoutInflater.from(parent.context), R.layout.item_venue, parent, false)
        binding.viewModel = viewModel
        binding.root.setOnClickListener {
            binding.viewModel?.let { clickCallback.invoke(it.venue) }
        }
        return binding
    }

    override fun bind(binding: ViewDataBinding, position: Int) {
        (binding as ItemVenueBinding).viewModel?.setItem(getItem(position))
        binding.executePendingBindings()
    }

}