package com.kuloglu.tech

import com.kuloglu.tech.di.component.ApplicationComponent
import com.kuloglu.tech.di.component.DaggerApplicationComponent
import com.kuloglu.tech.di.module.ApplicationModule

class App : android.app.Application() {

    val component: ApplicationComponent by lazy {
        DaggerApplicationComponent.builder()
                .applicationModule(ApplicationModule(this))
                .build()
    }
}

