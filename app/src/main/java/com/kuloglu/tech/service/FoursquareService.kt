package com.kuloglu.tech.service

import com.kuloglu.tech.core.Constant
import com.kuloglu.tech.model.apiModel.venueDetail.ResponseVenueDetail
import com.kuloglu.tech.model.apiModel.venueList.FoursquareResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface FoursquareService {
    @GET("venues/explore?client_id=${Constant.CLIENT_ID}&client_secret=${Constant.CLIENT_SECRET}&limit=${Constant.LIMIT}&v=${Constant.API_VERSION}")
    fun getNearVenues(@Query("query") query: String, @Query("near") near: String, @Query("ll") latLong: String): Observable<FoursquareResponse>


    @GET("venues/{id}?client_id=${Constant.CLIENT_ID}&client_secret=${Constant.CLIENT_SECRET}&v=${Constant.API_VERSION}")
    fun getVenueDetail(@Path("id") id: String): Observable<ResponseVenueDetail>
}