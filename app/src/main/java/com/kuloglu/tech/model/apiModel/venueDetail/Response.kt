package com.kuloglu.tech.model.apiModel.venueDetail

import com.google.gson.annotations.SerializedName

data class Response(
        @SerializedName("venue")
        val venue: Venue?
)