package com.kuloglu.tech.model.apiModel.venueDetail

import com.google.gson.annotations.SerializedName

data class ResponseVenueDetail(
        @SerializedName("response")
        val response: Response
)