package com.kuloglu.tech.model.apiModel.venueDetail

import com.google.gson.annotations.SerializedName

data class Venue(
        @SerializedName("id")
        val id: String,
        @SerializedName("location")
        val location: Location,
        @SerializedName("name")
        val name: String,
        @SerializedName("photos")
        val photos: Photos,
        @SerializedName("price")
        val price: Price,
        @SerializedName("rating")
        val rating: Double
)