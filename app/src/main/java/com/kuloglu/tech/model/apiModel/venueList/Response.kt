package com.kuloglu.tech.model.apiModel.venueList

import com.google.gson.annotations.SerializedName

data class Response(
        @SerializedName("groups")
        val groups: List<Group>
)