package com.kuloglu.tech.model.apiModel.venueList

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class Item(
        @SerializedName("venue")
        val venue: Venue? = null
) : Parcelable {
        constructor(parcel: Parcel) : this()

        override fun writeToParcel(parcel: Parcel, flags: Int) {

        }

        override fun describeContents(): Int {
                return 0
        }

        companion object CREATOR : Parcelable.Creator<Item> {
                override fun createFromParcel(parcel: Parcel): Item {
                        return Item(parcel)
                }

                override fun newArray(size: Int): Array<Item?> {
                        return arrayOfNulls(size)
                }
        }
}