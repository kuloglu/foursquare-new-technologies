package com.kuloglu.tech.model.apiModel.venueList

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class FoursquareResponse(@SerializedName("response")
                              val response: Response? = null) : Parcelable {
    constructor(parcel: Parcel) : this()

    override fun writeToParcel(parcel: Parcel, flags: Int) {

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<FoursquareResponse> {
        override fun createFromParcel(parcel: Parcel): FoursquareResponse {
            return FoursquareResponse(parcel)
        }

        override fun newArray(size: Int): Array<FoursquareResponse?> {
            return arrayOfNulls(size)
        }
    }
}