package com.kuloglu.tech.model.apiModel.venueList

import com.google.gson.annotations.SerializedName

data class Group(
        @SerializedName("items")
        val items: List<Item>
)