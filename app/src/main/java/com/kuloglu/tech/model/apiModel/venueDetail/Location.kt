package com.kuloglu.tech.model.apiModel.venueDetail

import com.google.gson.annotations.SerializedName

data class Location(
        @SerializedName("address")
        val address: String,
        @SerializedName("cc")
        val cc: String,
        @SerializedName("city")
        val city: String,
        @SerializedName("country")
        val country: String,
        @SerializedName("formattedAddress")
        val formattedAddress: List<String>
)