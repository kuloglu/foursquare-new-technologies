package com.kuloglu.tech.model

data class VenueDetail(
        var name: String,
        var photoUrl: String,
        var address: String,
        var userRate: String
)