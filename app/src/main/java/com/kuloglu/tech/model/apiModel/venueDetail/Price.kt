package com.kuloglu.tech.model.apiModel.venueDetail

import com.google.gson.annotations.SerializedName

data class Price(
        @SerializedName("currency")
        val currency: String,
        @SerializedName("tier")
        val tier: Int
)