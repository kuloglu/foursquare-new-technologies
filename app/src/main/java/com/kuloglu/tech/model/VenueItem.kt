package com.kuloglu.tech.model

data class VenueItem(
        var name: String,
        var id: String,
        var address: String
)