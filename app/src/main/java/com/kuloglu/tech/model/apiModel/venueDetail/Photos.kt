package com.kuloglu.tech.model.apiModel.venueDetail

import com.google.gson.annotations.SerializedName

data class Photos(
        @SerializedName("count")
        val count: Int,
        @SerializedName("groups")
        val groups: List<Group>,
        @SerializedName("summary")
        val summary: String
)