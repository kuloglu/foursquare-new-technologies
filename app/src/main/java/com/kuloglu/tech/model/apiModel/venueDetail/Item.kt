package com.kuloglu.tech.model.apiModel.venueDetail

import com.google.gson.annotations.SerializedName

data class Item(
        @SerializedName("id")
        val id: String,
        @SerializedName("suffix")
        val suffix: String
)