package com.kuloglu.tech.model.apiModel.venueDetail

import com.google.gson.annotations.SerializedName

data class Photo(
        @SerializedName("prefix")
        val prefix: String,
        @SerializedName("suffix")
        val suffix: String
)